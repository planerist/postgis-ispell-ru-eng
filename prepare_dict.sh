pushd dict

#en_us dictionaries
rm en_us*

wget http://src.chromium.org/svn/trunk/deps/third_party/hunspell_dictionaries/en_US.dic
wget http://src.chromium.org/svn/trunk/deps/third_party/hunspell_dictionaries/en_US.dic_delta
wget http://src.chromium.org/svn/trunk/deps/third_party/hunspell_dictionaries/en_US.aff -O en_us.affix

# Remove first line
sed -i '' 1d en_US.dic

# Concat the dic and dic_delta, sort alphabetically and remove the leading blank line (leaves the ending newline intact)
cat en_US.dic en_US.dic_delta | sort > en_us.dict
sed -i '' 1d en_us.dict

# Clean up source files
rm en_US*

#ru_ru dictionaries
rm russian*

wget http://src.chromium.org/svn/trunk/deps/third_party/hunspell_dictionaries/ru_RU.dic
wget http://src.chromium.org/svn/trunk/deps/third_party/hunspell_dictionaries/ru_RU.dic_delta
wget http://src.chromium.org/svn/trunk/deps/third_party/hunspell_dictionaries/ru_RU.aff -O ru_ru.affix

# Remove first line
sed -i '' 1d ru_RU.dic

# Concat the dic and dic_delta, sort alphabetically and remove the leading blank line (leaves the ending newline intact)
cat ru_RU.dic ru_RU.dic_delta | sort > ru_ru.dict
sed -i '' 1d ru_RU.dict

iconv -f koi8-r -t utf-8 < ru_RU.affix > russian.affix
iconv -f koi8-r -t utf-8 < ru_RU.dic > russian.dict

# Clean up source files
rm ru_RU*
rm ru_ru*

popd